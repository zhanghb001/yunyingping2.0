<?php

namespace app\upgrade\admin;

use app\system\admin\Admin;
use app\upgrade\model\UpgradeWeb;

class Bind extends Admin
{

    protected $oneModel = 'UpgradeWeb';

    protected function initialize()
    {
        parent::initialize();
        $this->UpgradeWeb = new UpgradeWeb;
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $keyword = input('keyword/s');
            $condition = [];
            $page = input('page/d', 1);
            $limit = input('limit/d', 15);
            $order = 'create_time DESC';
            $field = '';
            if ($keyword) {
                $condition[] = ['identifier', 'like', "%" . $keyword . "%"];
            }
            $result = $this->UpgradeWeb->pageList($condition, $field, $order, $page, $limit, ['username']);
            return $this->success('获取成功', '', $result);
        }
        return $this->fetch();
    }
}
