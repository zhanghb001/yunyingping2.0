<?php

/**
 * 模块菜单
 * 字段说明
 * url 【链接地址】格式：one_wxopen/控制器/方法，可填写完整外链[必须以http开头]
 * param 【扩展参数】格式：a=123&b=234555
 */
return [
    [
        'pid'           => 0,
        'title'         => '微信服务商',
        'icon'          => 'mdi mdi-shield-link-variant',
        'module'        => 'one_wxopen',
        'url'           => 'one_wxopen/index/index',
        'param'         => '',
        'target'        => '_self',
        'sort'          => 100,
        'childs' => [
            [
              'title' => '概览',
              'module' => 'one_wxopen',
              'url' => 'one_wxopen/index/index',
              'param' => '',
              'debug' => 0,
              'nav' => 1,
            ],
            [
              'title' => '授权列表',
              'module' => 'one_wxopen',
              'url' => 'one_wxopen/auth/index',
              'param' => '',
              'debug' => 0,
              'nav' => 1,
            ],
            [
                'title' => '模板管理',
                'module' => 'one_wxopen',
                'url' => 'one_wxopen/wemini/template',
                'param' => '',
                'debug' => 0,
                'nav' => 1,
                'childs' => [
                    [
                      'title' => '模板库',
                      'icon' => '',
                      'module' => 'one_wxopen',
                      'url' => 'one_wxopen/wemini/template',
                      'param' => '',
                      'target' => '_self',
                      'debug' => 0,
                      'system' => 0,
                      'nav' => 0,
                      'sort' => 0,
                    ],
                    [
                      'title' => '草稿箱',
                      'icon' => '',
                      'module' => 'one_wxopen',
                      'url' => 'one_wxopen/wemini/draft',
                      'param' => '',
                      'target' => '_self',
                      'debug' => 0,
                      'system' => 0,
                      'nav' => 0,
                      'sort' => 0,
                    ],
                ],
            ],
            [
              'title' => '相关配置',
              'module' => 'one_wxopen',
              'url' => 'one_wxopen/config/index',
              'param' => '',
              'debug' => 0,
              'nav' => 1,
            ],
            
        ],
    ],
];