/*
 sql安装文件
*/
CREATE TABLE `one_video_analysis`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `unique` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '唯一标识（视频id+源播放地址）',
  `source_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '源地址',
  `format_url` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '解析地址',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `unique`(`unique`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT = '视频解析索引';

CREATE TABLE `one_videos_danmu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `vid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '视频id',
  `index` tinyint(3) NOT NULL DEFAULT 0 COMMENT '剧集索引',
  `text` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '弹幕内容',
  `color` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '弹幕颜色',
  `time` float(10, 4) UNSIGNED NOT NULL DEFAULT 0.0000 COMMENT '弹幕时间',
  `create_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `update_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '视频弹幕' ROW_FORMAT = Dynamic;

CREATE TABLE `one_video_report`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vod_id` int(11) NOT NULL DEFAULT 0 COMMENT '视频id',
  `vod_play_from` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频源',
  `vod_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频名称',
  `vod_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频播放序列（第几集）',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注信息',
  `number` int(11) NOT NULL DEFAULT 1 COMMENT '反馈次数',
  `status` tinyint(3) NOT NULL DEFAULT 0 COMMENT '是否已处理 0 未处理 1 已处理',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic COMMENT = '视频反馈';

CREATE TABLE `one_video_replay` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `vod_id` int(11) NOT NULL DEFAULT '0' COMMENT '视频id',
  `vod_name` varchar(255) NOT NULL DEFAULT '' COMMENT '视频名称',
  `content` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '用户头像',
  `nickname` varchar(255) NOT NULL DEFAULT '' COMMENT '用户昵称',
  `status` tinyint(3) NOT NULL DEFAULT '1' COMMENT '状态 0 待审 1 通过',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='视频评论';

CREATE TABLE `one_video_subscribe`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT 0 COMMENT '用户id',
  `vod_id` int(11) NOT NULL DEFAULT 0 COMMENT '视频id',
  `vod_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '视频标题',
  `vod_time` int(11) NOT NULL DEFAULT 0 COMMENT '更新时间',
  `vod_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '5' COMMENT '评分',
  `vod_year` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '首播时间',
  `vod_actor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主演',
  `status` tinyint(3) NOT NULL COMMENT '状态 0 未推送 1 已推送',
  `create_time` int(11) NOT NULL DEFAULT 0,
  `update_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;