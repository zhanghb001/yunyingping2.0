<?php

namespace app\videos\server;

use app\common\server\Service;
use one\Http;

class Config extends Service
{
    public function initialize()
    {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg' => '非法操作！', 'code' => 712]));
        }
    }
    public function getConfig($data, $user)
    {
        $no_ad_limit = config('commoncfg.no_ad_limit');
        $no_ad_limit_arr = explode(',', $no_ad_limit);
        $no_ad_limit = count($no_ad_limit_arr) > 1 ? 1 : (int)$no_ad_limit;
        foreach ($no_ad_limit_arr as $key => $value) {
            $no_ad_limit_arr[$key] = (int)$value;
        }


        $data = [
            'ver' => config('one.version'),

            // 小程序订阅模版
            'template_id' => config('wxmptpl.template_id'),
            'wxmp_tpl_switch' => (int)config('wxmptpl.wxmp_tpl_switch'),
            'wxmp_tpl_Interval' => (int)config('wxmptpl.wxmp_tpl_Interval'),

            // 视频配置
            'copy_url' => (int)config('video.copy_url'),
            'video_poster' => config('video.video_poster'),
            'base_url' => config('video.base_url'),

            // 全局配置
            'kefu_qrcode' => config('commoncfg.kefu_qrcode'),
            'show_ad' => (int)config('commoncfg.show_ad'),
            'shake' => (int)config('commoncfg.shake'),
            'ad_switch' => (int)config('commoncfg.ad_switch'),
            'player_show_ad' => (int)config('commoncfg.player_show_ad'),
            'ad_close' => (int)config('commoncfg.ad_close'),
            'player_bottom_ad' => (int)config('commoncfg.player_bottom_ad'),
            'show_xx' => (int)config('commoncfg.show_xx'),
            'show_video_ad' => (int)config('commoncfg.show_video_ad'),
            'code_key' => config('commoncfg.code_key'),
            'member_group_switch' => (int)config('commoncfg.member_group_switch'),
            'member_group_limit' => (int)config('commoncfg.member_group_limit'),
            'login_bg_video' => config('commoncfg.login_bg_video'),
            'login_bg_text' => config('commoncfg.login_bg_text'),
            'wallpaper_button_text' => config('commoncfg.wallpaper_button_text'),
            'wallpaper_ad_lock' => (int)config('commoncfg.wallpaper_ad_lock'),
            'no_ad_limit' => $no_ad_limit,
            'no_ad_limit_arr' => $no_ad_limit_arr,
            'save_model' => (int)config('commoncfg.save_model'),
            'save_player' => config('commoncfg.save_player'),

            // 小程序设置
            'diy_tarbar' => (int)config('wechat.diy_tarbar'),
            'kefu_switch' => (int)config('wechat.kefu_switch'),
            'hot_search_key' => config('wechat.hot_search_key'),
            'share_img' => config('wechat.share_img'),
            'share_title' => config('wechat.share_title'),
            'wxwp_switch' => (int)config('wechat.wxwp_switch'),
            'wxmp_btn_type' => config('wechat.wxmp_btn_type'),
            'wxmp_img' => config('wechat.wxmp_img'),
            'wxmp_url' => config('wechat.wxmp_url'),
            'wxmp_btn_text' => config('wechat.wxmp_btn_text'),
            'tarbar_up' => (int)config('wechat.tarbar_up'),
            'gift_switch' => (int)config('wechat.gift_switch'),

            // 激励广告相关
            'play_start_ad' => (int)config('playad.play_start_ad'),
            'should_play_end' => (int)config('playad.should_play_end'),
            'play_ad_num' => (int)config('playad.play_ad_num'),
            'play_ad_switch' => (int)config('playad.play_ad_switch'),
            'play_ad_control' => (int)config('playad.play_ad_control'),
            'play_ad_title' => config('playad.play_ad_title'),
            'play_ad_text' => config('playad.play_ad_text'),
            'show_ad_half' => (int)config('playad.show_ad_half'),
            'show_ad_time' => (int)config('playad.show_ad_time'),

            // 弹窗设置
            'popup' => [
                'popup_type' => (int)config('popup.popup_type'),
                'show_popup' => (int)config('popup.show_popup'),
                'popup_img' => config('popup.popup_img'),
                'popup_title' => config('popup.popup_title'),
                'popup_msg' => config('popup.popup_msg'),
                'popup_dialog_title' => config('popup.popup_dialog_title'),
                'popup_dialog_content' => config('popup.popup_dialog_content'),
                'popup_dialog_confirm_text' => config('popup.popup_dialog_confirm_text'),
                'popup_dialog_close_text' => config('popup.popup_dialog_close_text'),
                'popup_dialog_confirm_type' => config('popup.popup_dialog_confirm_type'),
                'popup_dialog_confirm_appid' => config('popup.popup_dialog_confirm_appid'),
                'popup_dialog_confirm_page' => config('popup.popup_dialog_confirm_page'),
            ],

            // 悬浮窗
            'drag' => [
                'drag_btn_switch' => (int)config('commoncfg.drag_btn_switch'),
                'drag_btn_img' => config('commoncfg.drag_btn_img'),
                'drag_btn_type' => config('commoncfg.drag_btn_type'),
                'drag_btn_appid' => config('commoncfg.drag_btn_appid'),
                'drag_btn_page' => config('commoncfg.drag_btn_page'),
            ],

            'userinfo' => $user ? $user : []
        ];
        return $data;
    }

    public function updateInfo()
    {
        $data = [
            'ios' => [
                'version' => config('update_app.ios_version'),  // 后台填写的当前最新版本的app版本号，和客户端的globalConfigs.js里面配置的iosVersion如果相等则不跟新，如果不相等且atOnce为true时，会提示更新
                'download_url' => config('update_app.ios_download_url'), // 需要下载的url，此url为应用市场的下载地址
                'log' => config('update_app.ios_log'),  // 更新日志内容，有后台填写并返回
                'atOnce' => config('update_app.ios_atonce') == 0 ? false : true    // 如果有更新，是否立即提示更新，为false时不更新，为true时会提示更新，次提示可用于app上架审核期间控制不提示更新
            ],
            'android' => [
                'version' => config('update_app.android_version'),  // 后台填写的当前最新版本的app版本号，和客户端的globalConfigs.js里面配置的iosVersion如果相等则不跟新，如果不相等且atOnce为true时，会提示更新
                'download_url' => config('update_app.android_download_url'), // "http://sapp.lhave.net/download/fuapp.apk", 需要下载的url，此url为应用的下载地址
                'log' => config('update_app.android_log'),  // 更新日志内容，有后台填写并返回
                'atOnce' => config('update_app.android_atonce') == 0 ? false : true    // 如果有更新，是否立即提示更新，为false时不更新，为true时会提示更新，次提示可用于app上架审核期间控制不提示更新
            ]
        ];
        return $data;
    }

    /**
     * 生成海报
     *
     * @param [type] $data
     * @param [type] $qrcode
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function createPoster($data, $user)
    {
        // cache(md5($data['path']), null);
        // cache(md5($data['path']) . 'qrcode', null);
        // $image_path = cache(md5($data['path']));     // 海报图片目录
        // if ($image_path) {
        //     return $image_path;
        // }

        // $background = ROOT_PATH . 'public' . DS . config('wechat.poster_bg'); // 海报背景图
        // if (empty($background)) {
        //     $this->error = '请先上传海报背景图';
        //     return false;
        // }

        $qrcode_path = ROOT_PATH . 'public' . DS . 'upload' . DS . 'poster' . DS . 'qrcode' . DS;
        $qrcode_file_name = md5($data['path']) . '_qrcode_wx.png';
        if (!file_exists($qrcode_path)) {
            @mkdir($qrcode_path, 0777, true);
        }

        // $poster_path = ROOT_PATH . 'public' . DS . 'upload' . DS . 'poster' . DS . 'poster' . DS;
        // $poster_file_name = md5($data['path']) . '_poster.png';
        // if (!file_exists($poster_path)) {
        //     @mkdir($poster_path, 0777, true);
        // }

        // 获取小程序qrcode
        if (!($qrcode_file = cache(md5($data['path']) . 'qrcode'))) {
            //缓存access_token
            $ACCESS_TOKEN = cache('access_token');
            if (!$ACCESS_TOKEN) {
                //配置APPID、APPSECRET
                $APPID = config('wechat.appid');
                $APPSECRET = config('wechat.secret');
                //获取access_token
                $access_token = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$APPID&secret=$APPSECRET";
                $result = Http::get($access_token);
                $result = json_decode($result, true);
                if (isset($result['errcode']) && $result['errcode'] != 0) {
                    $this->error = $result['errmsg'];
                    return false;
                }
                if (!isset($result['access_token']) || empty($result['access_token'])) {
                    $this->error = '获取access_token失败！';
                    return false;
                }
                cache('access_token', $result['access_token'], 7200);
                $ACCESS_TOKEN = $result["access_token"];
            }

            //构建请求二维码参数
            //path是扫描二维码跳转的小程序路径，可以带参数?id=xxx
            //width是二维码宽度
            $qcode = "https://api.weixin.qq.com/wxa/getwxacode?access_token=$ACCESS_TOKEN";
            $param = [
                "path" => urldecode($data['path']),
                "width" => 100,
                'line_color' => ['r' => 61, 'g' => 188, 'b' => 202],
            ];
            //POST参数
            $result = Http::post($qcode, json_encode($param));
            if (json_decode($result, true)) {
                $this->error = $result['errmsg'];
                return false;
            }
            $qrcode_file = $qrcode_path . $qrcode_file_name;
            //生成二维码
            file_put_contents($qrcode_file, $result);
            cache(md5($data['path']) . 'qrcode', $qrcode_file);
        }

        if (!file_exists($qrcode_file)) {
            $this->error = '生成失败，请重新尝试';
            cache(md5($data['path']) . 'qrcode', null);
            return false;
        }

        return DS . 'upload' . DS . 'poster' . DS . 'qrcode' . DS . md5($data['path']) . '_qrcode_wx.png';

        // $showxx = config('commoncfg.show_xx');
        // $member_group_switch = config('commoncfg.member_group_switch');
        // $member_group_limit = config('commoncfg.member_group_limit');

        // $poster_title = '长按识别小程序码访问';
        // $poster_intro = '有想看的电影或电视剧，又不知道值不值得看？来这里看一下评分就知道啦！';
        // if ($user && isset($user['group'])) {
        //     if ($member_group_switch && $user['group'] >= $member_group_limit) {
        //         $poster_title = config('wechat.poster_title');
        //         $poster_intro = config('wechat.poster_intro');
        //     }
        // }

        // $font_path = ROOT_PATH . 'public' . DS . 'assets' . DS . 'fonts' . DS . 'fzltxh.ttf';
        // $data['title'] = mb_substr($data['title'], 0, 9);
        // $font_size = mb_strlen($data['title']) <= 8 ? 16 : 14;

        // $config = array(
        //     'image' => array(
        //         array(
        //             'url' => $qrcode_file,     //二维码地址
        //             'is_yuan' => true,          //true图片圆形处理
        //             'stream' => 0,
        //             'left' => 100,               //小于0为小平居中
        //             'top' => 220,
        //             'right' => 0,
        //             'width' => 150,             //图像宽
        //             'height' => 150,            //图像高
        //             'opacity' => 100            //透明度
        //         ),
        //         array(
        //             'url' => $data['img'],     // 视频封面
        //             'is_yuan' => false,          //true图片圆形处理
        //             'stream' => 0,
        //             'left' => 230,               //小于0为小平居中
        //             'top' => 25,
        //             'right' => 0,
        //             'width' => 100,            //图像宽
        //             'height' => 130,            //图像高
        //             'opacity' => 90            //透明度
        //         ),
        //     ),
        //     'text' => array(
        //         array(
        //             'text' => $data['title'],            //视频标题
        //             'left' => 20,                        //小于0为小平居中
        //             'top' => 50,
        //             'fontSize' => $font_size,                    //字号
        //             'fontColor' => '1,1,1',              //字体颜色
        //             'angle' => 0,
        //             'width' => 180,
        //             'fontPath' => $font_path,     //字体文件
        //         ),
        //         array(
        //             'text' => '简介：',            //文字内容
        //             'left' => 20,                              //小于0为小平居中
        //             'top' => 80,
        //             'fontSize' => 10,                         //字号
        //             'fontColor' => '1,1,1',                //字体颜色
        //             'angle' => 0,
        //             'width' => 180,
        //             'fontPath' => $font_path,     //字体文件
        //         ),
        //         array(
        //             'text' => mb_substr($poster_intro, 0, 72) . '...',            //文字内容
        //             'left' => 60,                              //小于0为小平居中
        //             'top' => 80,
        //             'fontSize' => 10,                         //字号
        //             'fontColor' => '1,1,1',                //字体颜色
        //             'angle' => 0,
        //             'width' => 150,
        //             'fontPath' => $font_path,     //字体文件
        //         ),
        //         array(
        //             'text' => $poster_title,            //说明文字，后台配置
        //             'left' => 20,                              //小于0为小平居中
        //             'top' => 700,
        //             'fontSize' => 16,                         //字号
        //             'fontColor' => '0,0,0',                //字体颜色
        //             'angle' => 0,
        //             'width' => 390,
        //             'fontPath' => $font_path,     //字体文件
        //         ),
        //         array(
        //             'text' => $poster_intro,            //说明文字，后台配置
        //             'left' => 10,                              //小于0为小平居中
        //             'top' => 730,
        //             'fontSize' => 14,                         //字号
        //             'fontColor' => '169,169,169',                //字体颜色
        //             'angle' => 0,
        //             'width' => 300,
        //             'fontPath' => $font_path,     //字体文件
        //         )
        //     ),
        //     'background' => $background,          //背景图
        // );
        // // 存放海报图片目录
        // $image_path = $poster_path . $poster_file_name;
        // //echo createPoster($config);
        // //$filename为空是真接浏览器显示图片
        // $image_path = createPoster($config, $image_path);
        // if ($image_path) {
        //     cache(md5($data['path']), $image_path);
        //     return $image_path;
        // }
        // return false;
    }
}
