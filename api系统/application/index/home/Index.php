<?php

namespace app\index\home;

use app\common\controller\Common;

use app\videos\model\Videos;

class Index extends Common
{
    public function index()
    {
        echo '<html><head><meta http-equiv="Content-Type"content="text/html; charset=UTF-8"/><meta http-equiv="X-UA-Compatible"content="IE=edge"/><meta name="viewport"content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/><meta name="robots"content="noarchive"/><title>每日一图 - 必应</title><style>h1{color:#FFFFFF;text-align:center;font-family:Microsoft Jhenghei}p{color:#FFFFFF;font-size:1.2rem;text-align:center;font-family:Microsoft Jhenghei}</style></head><body style="background: #FFFFFF url(index/index/img) no-repeat fixed center;"></body></html>';
        die;
    }

    public function img()
    {
        $str = file_get_contents('https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1');
        $str = json_decode($str, true);
        $imgurl = 'https://cn.bing.com' . $str['images'][0]['url'];
        header("Location: {$imgurl}");
        die;
    }

    /**
     * 预缓存视频
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function cache()
    {
        $map = [];
        $tid = input('tid', '');
        $tpid = input('tpid', '');
        $name = input('name', '');
        if ($tid) {
            $map[] = ['type_id', 'eq', $tid];
        }
        if ($tpid) {
            $map[] = ['type_id_1', 'eq', $tpid];
        }
        if ($name) {
            $map[] = ['vod_name', 'like', urldecode($name) . "%"];
        }
        // 白名单关键字
        $white_list = config('video.analysis_white_list');
        $white_list = trim($white_list);
        $white_list = explode("\n", $white_list);
        $model = new Videos();
        $list = $model->field('vod_name, vod_play_url, type_id, type_id_1')->where($map)->order('vod_year desc')->limit(0, 20)->select();
        // var_dump($list);
        $url = [];
        foreach ($list as $k => $v) {
            $purl = explode('#', $v['vod_play_url']);
            foreach ($purl as $n => $m) {
                $aa = explode('$', $m);
                $url[$n] = $aa[1];
            }
        }
        foreach ($white_list as $v) {
            foreach ($url as $as => $ds) {
                if (strstr($ds, $v)) {
                    unset($url[$as]);
                }
            }
        }
        $bz = input('bz', 0);
        $task = '';
        foreach ($url as $cxz) {
            $task .= 'http://jx.617kan.cn/king.php?url=' . $cxz . '&bz=' . $bz . "<br>";
        }
        echo $task;
    }
}
