import Base64Image from './images.js';
const MyApp = getApp();
const globalData = MyApp.globalData;
export default {
    data() {
        return {
            tvSendBg: Base64Image.tv_send_bg,
            maskBg: Base64Image.mask_bg,
            tvMaskBgStyle: '',
            tvIsShow: false,
            tvTitle: '正在搜寻设备...',
            tvMask: false,
            tvMaskTitle: '投屏中',
            tvBoxStyle: '',
            screenWidth: 750,
            isFullScreen: false,
            isDownloading: false,
            downloadTask: null,
            progressValue: 0, // 下载进度
            processBarWidth: 0,
            //倍速菜单
            rate: 'x1',
            //倍速文本,
            animation: false,
            animationMenu: true,
            videoContext: null,
            floatBoxStyle: '',
            floatRateBoxStyle: '',
            windowInfo: {
                statusBarHeight: 0,
                windowHeight: 0,
                windowWidth: 0,
                trueHeight: 0,
                screenHeight: 0
            },
            modalName: '',
            hideTime: 6,
            isFirst: true,
            timer: null,
            timerOfLock: null,
            lockBoxStyle: '',
            rememberBoxStyle: '',
            isLockScreen: false,
            isTapChoose: false,
            isShowlockButton: true,
            playFirst: true, //第一次播放
            remeber: false, //播放记忆
            play: false, //播放状态
            duration: 0, //精确总时长
            os: uni.getSystemInfoSync().platform,
            showTitle: true,
            showVideoBox: false,

            showRight: false,
            playingCurrent: 0,
            // 倍速播放索引
            playbackRateIndex: 2,
            // 倍速设置选项列表
            playbackRateList: ['0.5', '0.8', '1.0', '1.2', '1.5', '2.0'],
            fill: 'contain',
            VodLoadText: globalData.$t('detail').vodLoadText,
            flotage: '',
            flotageY: '',
            rightText: '<span style="color: #FFFFFF;font-size: 26upx;"><i class="uniicons beisu"></i>213</span>'
        };
    },
    created() {
        this.screenWidth = uni.getSystemInfoSync().windowWidth;
        const res = uni.getSystemInfoSync();
        this.windowInfo.statusBarHeight = res.statusBarHeight;
        this.windowInfo.windowHeight = res.windowHeight;
        this.windowInfo.windowWidth = res.windowWidth;
        this.windowInfo.screenHeight = res.screenHeight;
        this.windowInfo.trueHeight = this.windowInfo.windowHeight - this
            .windowInfo.statusBarHeight - 50;
        this.initFloatBox();
        this.videoContext = uni.createVideoContext('videos', this);
        this.maskSize();
        // this.countDown();
        // this.screenLockCountDown();
    },
    computed: {
        rememberCurrent() {
            return this.formatSeconds(this.initialTime);
        },
        i18n() {
            return globalData.$t('detail');
        }
    },
    watch: {
        playFirst: {
            handler: async function(newVal, oldVal) {
                if (newVal) {
                    this.remeber = true;
                    await this.promise(10000);
                    this.remeber = false;
                }
            }
        },
        isFullScreen: {
            handler: async function(newVal, oldVal) {
                if (newVal) {
                    this.remeber = true;
                    await this.promise(5000);
                    this.remeber = false;
                }
            }
        },
        url: {
            handler: async function(newVal, oldVal) {
                let _this = this;
                if (newVal) {
                    _this.tvMaskBgStyle = 'height:' + _this.videoHeight +
                        'px; width: ' + _this.windowInfo.windowWidth +
                        'px;';
                    setTimeout(v => {
                        if (!_this.showxx) {
                            _this.changSrc(newVal);
                        } else {
                            _this.isLockScreen = true;
                        }
                    }, 1000)
                }
            },
            deep: true
        },
    },
    mounted() {
        let _this = this;
        _this.$nextTick(function() {
            uni.$on('showTvMask', function() {
                _this.tvMask = true;
                setTimeout(function() {
                    _this.videoContext.pause();
                }, 500)
            })

            uni.onWindowResize((res) => {
                _this.animationMenu = true;
                _this.isLockScreen = false;
            })
        })
    },
    methods: {
        // 视频是否填充
        fillTap() {
            if (this.fill == 'fill') {
                this.fill = 'contain';
            } else {
                this.fill = 'fill';
            }
        },
        // 快进按钮
        speed(type) {
            if (type == 'jia') {
                this.videoContext.seek(parseInt(this.playingCurrent) + 30);
            } else {
                this.videoContext.seek(parseInt(this.playingCurrent) - 30);
            }
        },
        // 设置倍速播放
        playbackRate() {
            if (this.playbackRateIndex == this.playbackRateList.length - 1) {
                this.playbackRateIndex = 0;
                this.videoContext.playbackRate(Number(this.playbackRateList[this
                    .playbackRateIndex]));
                return;
            }
            this.playbackRateIndex++;
            this.videoContext.playbackRate(Number(this.playbackRateList[this
                .playbackRateIndex]));
        },
        goPage() {
            this.videoContext.exitFullScreen();
            uni.showLoading({
                mask: true
            })
            setTimeout(function() {
                uni.navigateTo({
                    url: '/packageA/pages/user/child/giftCode',
                    success() {
                        uni.hideLoading();
                    }
                })
            }, 1000)
        },
        showVideo() {
            this.showVideoBox = true;
        },
        pauseVideo() {
            this.videoContext.stop();
        },
        goback() {
            this.videoContext.exitFullScreen();
        },
        formatSeconds(a, type) {
            var hh = parseInt(a / 3600);
            var mm = parseInt((a - hh * 3600) / 60);
            if (mm < 10) mm = "0" + mm;
            var ss = parseInt((a - hh * 3600) % 60);
            if (ss < 10) ss = "0" + ss;
            if (hh < 10) hh = hh == '0' ? '' : `0${hh}:`;
            var length = hh + mm + ":" + ss;
            if (a >= 0) {
                return length;
            } else {
                return "00:00";
            }
        },
        // 播放控制
        async changSrc(url) {
            this.videoContext.stop();
            this.remeber = this.initialTime && true;
            this.playFirst = true;
            await this.promise(300);
            this.playFirst = false;
            this.url = url;
            await this.promise(300);
            this.videoContext.play();
            this.isLockScreen = false;
            this.play = true;
            this.videoContext.seek(0);
            this.videoContext.playbackRate(this.rate * 1);
            await this.promise(10000);
            this.remeber = false;
            this.play = false;
        },
        //跳转记忆
        async tapRemeber(e) {
            e.stopPropagation();
            this.videoContext.seek(this.initialTime);
            await this.promise(100);
            this.remeber = false;
        },
        promise(time = 0) {
            let promise = new Promise((resolve, reject) => {
                setTimeout(() => {
                    resolve();
                }, time);
            });
            return promise;
        },
        initFloatBox() {
            if (this.isFullScreen) {
                this.animationMenu = true;
                this.isShowlockButton = true;
                this.iosFloatBoxFullScreenInit();
            } else {
                this.animationMenu = true;
                this.tvBoxStyle =
                    'position: fixed; flex-direction: row;justify-content: flex-end;top: 30upx;right: 30upx;z-index:3;';
                this.iosFloatBoxInit();
            }
        },
        // 全屏ios
        iosFloatBoxFullScreenInit() {
            this.lockBoxStyle = 'top: ' + (this.windowInfo.windowWidth / 2 -
                    20) * 2 + 'upx; left: ' +
                (this.windowInfo.statusBarHeight * 2 + 30) +
                'upx; border-radius: 50;width: 100upx;height: 100upx;';
            this.floatBoxStyle = 'top: ' + (this.windowInfo.windowWidth / 2 -
                28) * 2 + 'upx;';
            this.floatRateBoxStyle = 'top: ' + ((this.windowInfo.windowWidth /
                2) * 2 - 60) + 'upx;';
            this.rememberBoxStyle = 'top: ' + ((this.windowInfo.windowWidth) + (
                    this.windowInfo.windowWidth / 2.5)) +
                'upx; left: ' + (this.windowInfo.statusBarHeight * 2 + 10) +
                'upx';
            this.titleStyle = 'width: ' + (this.windowInfo.screenHeight) + 'px;';
            this.tvBoxStyle =
                'position: absolute; flex-direction: row;justify-content: flex-end;top: 30upx;right: 30upx;z-index:3;';
        },
        // 非全屏ios
        iosFloatBoxInit() {
            this.floatBoxStyle = 'top: ' + (this.videoHeight / 1.22) + 'upx;';
            this.floatRateBoxStyle = 'top: ' + (this.videoHeight / 1.3) +
                'upx;';
            this.rememberBoxStyle = 'left: 20upx; top: ' + (this.videoHeight + (
                this.videoHeight / 5)) + 'upx;';

            this.flotage = 'top: ' + (this.videoHeight + 120) + 'upx;';

            this.flotageY = 'top:' + (this.videoHeight + 30) + 'upx;';
        },
        changeScreenLock() {
            this.isLockScreen = !this.isLockScreen;
            this.hideTime = 6;
            this.screenLockCountDown();
        },
        screenLockCountDown() {
            let _this = this;
            clearTimeout(_this.timerOfLock);
            if (_this.hideTime <= 0) {
                _this.isShowlockButton = false;
                return;
            }
            _this.hideTime--;
            this.timerOfLock = setTimeout(_this.screenLockCountDown, 1000);
        },
        videoClick() {
            this.hideTime = 6;
            this.isShowlockButton = true;
        },
        countDown() {
            let _this = this;
            clearTimeout(_this.timer);
            _this.hideTime--;
            if (_this.hideTime <= 0) {
                _this.hideMenu();
                return;
            }
            this.timer = setTimeout(_this.countDown, 1000);
        },
        async sendtv() {
            // 如果是全屏，先退出全屏
            if (this.isFullScreen) {
                this.videoContext.exitFullScreen();
                await this.promise(300);
            }
            this.tvIsShow = true;
            this.$refs.tvSearch.tvInit();
        },
        exittv() {
            this.tvIsShow = false;
            this.tvMask = false;
            this.$refs.tvSearch.stopVideos();
            this.videoContext.play();
        },
        changeSpeed() {
            this.animationMenu = false;
            this.animation = true;
            this.isTapChoose = true;
            // this.countDown();
        },
        showMenu() {
            if (this.animation) {
                return;
            }
            this.animationMenu = true;
        },
        hideMenu() {
            this.animationMenu = false;
            this.animation = false;
            this.isTapChoose = false;
        },
        //倍速选择
        choseRate(e) {
            let rate = Number(e.target.dataset.rate);
            if (rate == 0) {
                this.playNext();
                return false;
            }
            this.videoContext.playbackRate(rate);
            this.rate = 'x' + rate;
            this.initFloatBox();
            this.animationMenu = true;
            this.animation = false;
        },
        controlstoggle(e) {
            if (e.detail.show) {
                // this.countDown();
                this.showMenu();
                this.showTitle = true;
                this.isShowlockButton = true;
                this.hideTime = 6;
            } else {
                this.screenLockCountDown();
                if (!this.isTapChoose) {
                    this.hideMenu();
                }
                this.showTitle = false;
                if (!this.isLockScreen) {
                    this.isShowlockButton = false;
                }
            }
        },
        showToast(title) {
            uni.showToast({
                title: title,
                icon: 'none'
            });
        },
        playNext() {
            this.isLockScreen = true;
            this.showRight = false;
            this.videoContext.stop();
            uni.$emit('playNext', {
                isFullScreen: this.isFullScreen
            });
        },
        timeUpdate(event) {
            // this.processBarWidth = (e.detail.currentTime / e.detail.duration) * this.screenWidth;
            // this.duration = event.detail.duration;
            // this.current = event.detail.currentTime;
            this.playingCurrent = event.detail.currentTime;
            this.showRight = true;
            uni.$emit('timeUpdate', event);
        },
        setFullScreenStatus(e) {
            this.animationMenu = false;
            if (e.detail.direction == 'horizontal') {
                this.isFullScreen = true;
                this.isShowlockButton = false;
                this.animationMenu = false;
                this.animation = false;
            } else {
                this.isFullScreen = false;
            }
            let _this = this;
            setTimeout(function() {
                _this.initFloatBox();
                if (_this.showxx) {
                    _this.maskSize();
                }
            }, 300);
        },
        maskSize() {
            let windowHeight, width, videoHeight, statusBarHeight, trueHeight, height;
            const res = uni.getSystemInfoSync();
            statusBarHeight = res.statusBarHeight;
            width = res.windowWidth;
            windowHeight = res.windowHeight;
            height = res.screenHeight;
            trueHeight = windowHeight - statusBarHeight - 50;
            if (!this.isFullScreen) {
                videoHeight = trueHeight / 3;
            } else {
                videoHeight = height;
            }
            this.videoStyle = 'width: ' + width + 'px;height:' + videoHeight + 'px;';
            this.tvMaskBgStyle = 'height:' + videoHeight +
                'px; width: ' + width + 'px;';
        }
    }
};
