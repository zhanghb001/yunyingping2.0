import hh from './interface';
import aa from '../config.js';
import jj from '../md5.js';
import td from './bd.js';

var sys_info = uni.getSystemInfoSync();
export let post = (url, data) => {
    let timestamp = new Date().getTime();
    let token = uni.getStorageSync('token');
    let _s = st(data);
    //设置请求前拦截器
    hh.interceptor.request = (config) => {
        let u = aa.websiteUrl.split('/'),
            _si = jj('key=' + aa.websiteSecret + '&' + _s + '&timestamp=' + (parseInt(timestamp *
                1000)) + u[2]);
        _si = _si.substring(5, 26).toUpperCase();
        config.header = {
            'secret': aa.websiteSecret,
            'token': token,
            'timestamp': timestamp,
            't': _si,
            'Did': sys_info.deviceId
        };
        config.method = 'POST';
        config.data = {
            'body': ee(uc(config.data), timestamp, aa.websiteSecret)
        };
    };
    //设置请求结束后拦截器
    hh.interceptor.response = (response) => {
        if (response.data.code >= 400) {
            if (1200 == response.data.code) {
                uni.setStorageSync('userInfo', null);
                let sys = uni.getStorageSync('sys_config');
                let login_page = uni.getStorageSync('login_page');
                if (!sys.show_xx) {
                    uni.navigateTo({
                        url: login_page
                    })
                }
                return false;
            }
            if (response.data.code == 910 || response.data.code == 810) {
                return false;
            }
            if (response.data.code >= 710 && response.data.code < 713) {
                return false;
            }
            uni.showToast({
                title: response.data.msg || "系统繁忙",
                duration: 1300,
                icon: 'none'
            })
            return false;
        }
        if (response.data.code > 100 && response.data.code < 199) {
            uni.showModal({
                title: '提示',
                content: response.data.msg || "请求失败",
                showCancel: false
            })
            return false;
        }
        if (response.data.data && !response.data.data.hasOwnProperty('_format')) {
            response.data.data = dd(response.data.data, response.config.url);
        }
        // console.log('返回：');
        // console.log(response.data.data);
        return response;
    };
    // console.log('请求：');
    // console.log(data);
    return hh.request({
        url: url,
        data
    });
};
export let get = (url, data) => {
    let timestamp = new Date().getTime();
    let token = uni.getStorageSync('token');
    let _s = st(data);
    //设置请求前拦截器
    hh.interceptor.request = (config) => {
        let u = aa.websiteUrl.split('/'),
            date = new Date(),
            _si = jj('key=' + aa.websiteSecret + '&' + _s + '&timestamp=' + (parseInt(timestamp *
                1000)) + u[date.toString().split(' ')[3].slice(0, 1)]);
        _si = _si.substring(5, 26).toUpperCase();
        config.header = {
            'secret': aa.websiteSecret,
            'token': token,
            'timestamp': timestamp,
            't': _si,
            'Did': sys_info.deviceId
        };
        config.method = 'GET';
        config.data = {
            'body': ee(uc(config.data), timestamp, aa.websiteSecret)
        };
    }
    //设置请求结束后拦截器
    hh.interceptor.response = (response) => {
        if (response.data.code >= 400) {
            if (1200 == response.data.code) {
                uni.setStorageSync('userInfo', null);
                let sys = uni.getStorageSync('sys_config');
                let login_page = uni.getStorageSync('login_page');
                if (!sys.show_xx) {
                    uni.navigateTo({
                        url: login_page
                    })
                }
                return false;
            }
            if (response.data.code == 910 || response.data.code == 810) {
                return false;
            }
            if (response.data.code >= 710 && response.data.code < 713) {
                return false;
            }
            uni.showToast({
                title: response.data.msg || "系统繁忙",
                duration: 1300,
                icon: 'none'
            })
            return false;
        }
        if (response.data.code > 100 && response.data.code < 199) {
            uni.showModal({
                title: '提示',
                content: response.data.msg || "请求失败",
                showCancel: false
            })
            return false;
        }
        response.data.data = dd(response.data.data, url);
        return response;
    }
    return hh.request({
        url: url,
        data
    });
}
export default {
    post,
    get
};
const st = (data) => {
    var ret = [];
    for (let it in data) {
        let val = data[it];
        if (typeof val === 'object' &&
            (!(val instanceof Array) || (val.length > 0 && ('object' === typeof val[0])))) {
            val = JSON.stringify(val);
        }
        ret.push((it + "=" + val));
    }
    // 字典升序
    ret.sort();
    return ret.join('&');
};
const uc = (params) => {
    var _result = [];
    for (var key in params) {
        var value = params[key];
        if (value && value.constructor == Array) {
            value.forEach(function(_value) {
                _result.push(key + "=" + _value);
            });
        } else {
            _result.push(key + '=' + value);
        }
    }
    return _result.join('&');
};

const ee = (uncrypted, timestamp, secret) => {
    let as = timestamp + jj(secret);
    if (uncrypted == '') return td.en(as, jj(as));
    return td.en(uncrypted, jj(jj(sys_info.deviceId + secret + timestamp).substr(6, 18)));
};

const dd = (data, url) => {
    return td.de(data, url, sys_info.deviceId);
};
