const MyApp = getApp();
const globalData = MyApp.globalData;
export default {
    computed: {
        i18n() {
            return globalData.$t;
        }
    },
    data() {
        return {
            tarbarObj: globalData.tarbarObj,
            typelist: globalData.typelist,
            defaultKw: uni.getStorageSync('sys_config').hot_search_key || '',
            kwList: [],
            hotKwList: '',
            times: 0,
            loading: false,
            page: 1,
            limit: 4,
            isEnd: false,
            kw: '',
            scrollStyle: '',
            adSwitchKg: globalData.adSwitchKg || uni.getStorageSync('sys_config').ad_switch,
            oneAd: MyApp.getOneAd('common_ad'),
        }
    },
    onReady() {
        let _this = this;
        uni.getSystemInfo({
            success: res => {
                let query = uni.createSelectorQuery().in(_this);
                query.select('#uTabbar').boundingClientRect();
                query.exec(re => {
                    let uTabbarHeight = re[0].height;
                    _this.scrollStyle = 'height:' + (res.windowHeight - res.statusBarHeight -
                        uTabbarHeight - uni.upx2px(215)) + 'px;';
                });
            }
        });
        this.clearkwList();
        this.loadHotKw();
    },
    methods: {
        setListByKw(kw) {
            let _this = this;
            _this.kw = kw;
            _this.kwList = [];
            _this.page = 1;
            clearTimeout(_this.times);
            _this.times = setTimeout((v) => {
                _this.getKeywordList(kw)
            }, 200)
        },
        clearkwList() {
            this.kwList = [];
            this.isEnd = false;
            this.page = 1;
            this.limit = 4;
            this.kw = '';
            this.loading = false;
            clearTimeout(this.times);
        },
        //加载热门搜索
        loadHotKw() {
            let _this = this;
            _this.$http
                .post('/videos/videos/hotKeyWords', {
                    page: _this.page,
                    limit: 20
                })
                .then(res => {
                    _this.hotKwList = res.data.list;
                })
        },
        goDetail(item) {
            MyApp.godetail(item.vod_id);
        },
        getKeywordList(kw) {
            let _this = this;
            if (this.loading) {
                return false;
            }
            _this.loading = true;
            _this.$http
                .post('/videos/videos/searchKeywords', {
                    keyword: kw || _this.kw,
                    page: _this.page,
                    limit: _this.limit
                })
                .then(res => {
                    _this.loading = false;
                    if (res.code == 200) {
                        if (res.data.list.length > 0) {
                            res.data.list.forEach((v, k) => {
                                _this.kwList.push(v);
                            });
                        } else {
                            _this.isEnd = true;
                        }
                    }
                })
        },
        onreachBottom() {
            this.page = this.page + 1;
            this.limit = 4;
            if (!this.isEnd) {
                this.getKeywordList();
            }
        }
    }
}
