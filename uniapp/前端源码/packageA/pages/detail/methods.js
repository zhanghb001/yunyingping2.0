// 小程序插屏广告
let interstitialAd = null;
// 小程序激励广告
let rewardedVideoAd = null;
const MyApp = getApp();
const globalData = MyApp.globalData;
import hex_md5 from '@/common/md5.js';
import Base64Image from '../../static/images.js';
import util from '@/utils/utils.js';
export default {
    computed: {
        i18n() {
            return globalData.$t('detail');
        }
    },
    data() {
        return {
            poster: '',
            xuanjiSortName: 'sort-down',
            showCopy: 0,
            tabIndex: 0,
            typelist: globalData.typelist,
            showControls: true,
            // 视频加载字
            VodLoadText: globalData.$t('detail').vodLoadText,
            // 是否显示视频控件
            isShowControl: false,
            // 视频是否填充
            fill: 'contain',
            // 倍速播放索引
            playbackRateIndex: 2,
            // 倍速设置选项列表
            playbackRateList: ['0.5', '0.8', '1.0', '1.2', '1.5', '2.0'],
            cssAnimation: false,
            isMp: true,
            isShowxx: true,
            loadingBoxText: globalData.$t('detail').loadingText,
            videoAd: '',
            imageUrl: this.$config.imageUrl,
            apiImageUrl: this.$config.apiImageUrl,
            showAdModel: false,
            playerADStyle: '',
            bodyMinHeight: '',
            statusMaskTop: '',
            scrollLeft: 0,
            TabCur: 0,
            isback: false,
            isLike: false,
            videoInfo: {
                srcList: [{
                    name: '',
                    url: ''
                }],
                recommend: [],
                title: '',
                playIndex: 0,
                liveMode: false,
                remark: '',
                type: 0,
                currentTime: 0,
                danmuList: [],
                downLoadList: [],
                vod_pic: ''
            },
            vod_id: 0,
            videoType: 0,
            vid: 0,
            videoStyle: '',
            videoUrl: '',
            vodPlayFrom: '',
            vodPlayFromTab: '',
            currentTime: 0,
            playCurrentTime: 0,
            videoContext: null,
            gobackView: null,
            windowInfo: {
                statusBarHeight: 0,
                windowHeight: 0,
                windowWidth: 0,
                trueHeight: 0
            },
            contentStyle: '',
            playerAd: {
                img: '',
                url: ''
            },
            playerBottomAD: {
                img: '',
                url: '',
                isshow: 0
            },
            playerTopAD: {
                img: '',
                url: '',
                isshow: 0
            },
            customAD: {
                img: '',
                url: '',
                isshow: 0
            },
            isShowVideoTopAd: false,

            isIos: this.$os == 'ios',
            isPlaying: false,
            isShowPoster: true,
            isPause: false,
            showXuanji: false,
            xuanjiMask: false,
            isFullscreen: false,

            //倍速菜单
            rate: 'x1',
            //倍速文本,
            animation: null,
            animationMenu: null,
            showRate: true,
            adInfo: uni.getStorageSync('adInfo') || [],
            kefuSwitch: uni.getStorageSync('sys_config').kefu_switch || 0,
            watingNum: 0,
            showAdFirst: 0,
            sysconf: null,
            showAdHalf: false,
            halfTimeShowAd: 900,
            changeYuan: false,
            trueVideoUrl: '',
            navTitle: '',
            member_group: 0,
            wxmp_btn_img: Base64Image.wxmp_btn,
            // 状态栏高度，H5中，此值为0，因为H5不可操作状态栏
            statusBarHeight: uni.getSystemInfoSync().statusBarHeight,
            // 导航栏内容区域高度，不包括状态栏高度在内
            navbarHeight: 44,
            loading: true,

            playAdControl: 1,
            isClickCancel: false,
            isAdEnd: false,
            isAdShow: false,

            adTimer: null,
            adStep: 1,
            adCountTime: 5,

            hideTime: 0,

            watingPlayTimer: null,
            watingNum: 1,
            playingNum: 1,

            isShowModel: false,

            windowSizeWidth: 0,

            picModel: uni.getStorageSync('pictureModel') ? ['pop'] : [],

            saveModel: 0
        };
    },
    onReady() {
        let _this = this;
        // _this.$refs.loading.open();
    },
    watch: {
        videoUrl(newVal, oldVal) {
            let _this = this;
            if (!_this.isShowxx) {
                if (this.isFullscreen) {
                    uni.showLoading({
                        mask: true,
                        title: _this.loadingBoxText
                    });
                } else {
                    MyApp.showLoading(_this.$refs.loading);
                }

                if ((newVal && oldVal == '') || _this.changeYuan) {
                    _this.showControls = false;
                    setTimeout(function() {
                        _this.getAnalysis(newVal);
                    }, 300);
                }
            }
        }
    },
    onLoad(option) {
        this.adInfo = uni.getStorageSync('adInfo') || 0;
        this.vid = option.id;
        if (!this.vid) {
            uni.showModal({
                title: globalData.$t('common').modelTitle,
                content: globalData.$t('common').noSearchInfo,
                showCancel: false,
                success() {
                    uni.navigateBack({});
                }
            });
            return false;
        }
        if (option.hasOwnProperty('tab')) {
            this.TabCur = option.tab;
        }
        if (option.hasOwnProperty('froms')) {
            this.froms = 1;
        }
        this.videoContext = uni.createVideoContext('myVideo');
        if (this.videoContext != null) {
            if (option.hasOwnProperty('index')) {
                this.TabCur = option.index;
            }
        }
        if (this.videoInfo) {
            this.currentTime = uni.getStorageSync(hex_md5(this.videoInfo.title +
                this.TabCur));
        }
        this.getAdinfo();
    },
    onShow() {
        // let watchAdTime = parseInt(new Date().getTime() / 1000 - this.hideTime);
        // console.log(watchAdTime);
        // if (this.hideTime > 0) {
        //     if (watchAdTime < this.adCountTime) {
        //         console.log('没看完');
        //         this.playAdControl = 1;
        //         this.hideTime = 0;
        //     } else {
        //         this.playAdControl = 0;
        //         console.log('看完了' + this.playAdControl);
        //     }
        // }
        this.tabIndex = uni.getStorageSync('tabIndex') || 0;
    },
    onHide() {
        let _this = this;
        // // 如果用户点了广告去看了，不再限制播放
        // if (_this.isAdShow) {
        //     // 记录当前时间
        //     _this.hideTime = parseInt(new Date().getTime() / 1000);
        // }
        _this.videoContext.pause();
    },
    onUnload() {
        this.showAdModel = false;
        this.share.hard = 0;
        // 监听window尺寸变化
        this.listenFullScreen()
    },
    mounted() {
        let _this = this;
        uni.$on('showLoad', function(data) {
            _this.loadingBoxText = data.text;
            MyApp.showLoading(_this.$refs.loading);
        })
        uni.$on('closeLoad', function() {
            MyApp.closeLoading(_this.$refs.loading);
        })
        // 监听window尺寸变化
        _this.listenFullScreen()
    },
    methods: {
        listenFullScreen() {
            let _this = this;
            if (_this.$u.sys().model == 'microsoft' || _this.$u.sys()
                .platform ==
                'windows') {
                uni.onWindowResize((res) => {
                    if (_this.windowSizeWidth != 0) {
                        if (_this.windowSizeWidth > res.size
                            .windowWidth) {
                            _this.showXuanji = false;
                            _this.isFullscreen = false;
                            _this.windowSizeWidth = res.size
                                .windowWidth;
                            _this.isShowPoster = true;
                            _this.currentTime = _this.playCurrentTime;
                            setTimeout(v => {
                                _this.videoContext
                                    .exitFullScreen();
                                _this.isShowPoster = false;
                            }, 300);
                        } else {
                            _this.windowSizeWidth = res.size
                                .windowWidth;
                        }
                    } else {
                        _this.windowSizeWidth = res.size.windowWidth;
                    }
                })
            }
        },
        watingTimer() {
            let _this = this;
            if (_this.watingNum >= 20) {
                if (_this.playingNum <= 5 && !_this.saveModel) {
                    _this.$refs.uTips.show({
                        title: '若长时间未播放，换个播放源试试~┌(。Д。)┐',
                        type: 'warning'
                    });
                }
                _this.playingNum = 1;
                clearTimeout(_this.watingPlayTimer);
                return true;
            }
            _this.watingNum = _this.watingNum + 1;
            try {
                _this.watingPlayTimer = setTimeout(v => {
                    _this.watingTimer();
                }, 1000);
            } catch (e) {
                console.log(e);
            }
        },
        getAnalysis: function(newVal) {
            let _this = this;
            try {
                _this.$http
                    .post('/videos/analysis/getAnalysis', {
                        url: _this.tools.encode(encodeURIComponent(
                            newVal)),
                        vid: _this.vod_id,
                        vtitle: _this.videoInfo.title
                    })
                    .then(res => {
                        _this.showControls = true;
                        _this.isShowPoster = false;

                        if (res.code != 200) {
                            return false;
                        }
                        if (!_this.saveModel) {
                            _this.$refs.uTips.show({
                                title: '♡握手成功♡ ヽ(=^･ω･^=)丿',
                                type: 'warning'
                            });
                        }

                        _this.trueVideoUrl = res.data;
                        _this.changeYuan = false;
                        _this.showAdFirst = _this.sysconf.play_start_ad;
                        _this.showAdHalf = _this.sysconf.show_ad_half;
                        _this.isClickCancel = false;
                        _this.playAdControl = 1;
                        if (_this.saveModel) {
                            _this.copyVideoUrl();
                            return true;
                        }
                        _this.videoContext.play();
                        _this.watingTimer();
                    }).then(v => {
                        uni.hideLoading();
                        MyApp.closeLoading(_this.$refs.loading);
                        _this.loading = false;
                    })
                    .catch(res => {
                        uni.hideLoading();
                        _this.$refs.uTips.show({
                            title: '失败了~┌(。Д。)┐，换个线路试试',
                            type: 'warning'
                        });
                        MyApp.closeLoading(_this.$refs.loading);
                        _this.loading = false;
                    });
            } catch (e) {
                uni.hideLoading();
                _this.$refs.uTips.show({
                    title: '失败了~┌(。Д。)┐，换个线路试试',
                    type: 'warning'
                });
                MyApp.closeLoading(_this.$refs.loading);
                _this.loading = false;
                _this.$u.toast('请求超时，请重新尝试');
            }
        },
        xuanjiSort() {
            this.videoInfo.srcList[this.vodPlayFromTab] = this.videoInfo
                .srcList[this.vodPlayFromTab].reverse();
            this.xuanjiSortName = this.xuanjiSortName == 'sort-down' ?
                'sort-up' : 'sort-down';
            this.TabCur = -1;
        },
        changeXuanjiMask() {
            this.xuanjiMask = !this.xuanjiMask;
            this.isShowControl = false;
        },
        chooseXuanji() {
            this.isShowControl = false;
            this.xuanjiMask = true;
        },
        parseLoaded() {
            this.$refs.uReadMore.init();
        },
        // 视频元数据加载完成时触发。event.detail = {width, height, duration}
        loadedmetadata(e) {
            this.videoContext.play();
            this.VodLoadText = '';
        },
        // 快进按钮
        speed(type) {
            if (type == 'jia') {
                this.videoContext.seek(parseInt(this.playCurrentTime) + 30);
            } else {
                this.videoContext.seek(parseInt(this.playCurrentTime) - 20);
            }
        },
        fullscreenchange(e) {
            if (e.detail.fullScreen) {
                this.showXuanji = true;
                this.isFullscreen = true;
                if (this.$u.sys().model == 'microsoft' || this.$u.sys()
                    .platform ==
                    'windows') {
                    let alert = uni.getStorageSync('alertFullTip');
                    if (!alert) {
                        uni.showModal({
                            title: 'PC模式下小程序使用提示',
                            content: '该提示只显示1次，请仔细阅读！因PC模式下微信的退出全屏有bug，请按键盘上的Esc键或者点击视频右下角按钮（进度条最右边）退出全屏模式，请勿点击视频左上角的返回按钮！',
                            showCancel: false,
                            success() {
                                uni.setStorageSync('alertFullTip', 1);
                            }
                        })
                    }
                }
            } else {
                this.showXuanji = false;
                this.isFullscreen = false;
            }
        },
        //
        fullscreenclick(e) {
            console.log(e);
        },
        // 视频出现缓冲时触发
        waiting(e) {
            this.VodLoadText = globalData.$t('detail').bufferText;
            this.isPause = true;
            this.isPlaying = false;
        },
        // 是否显示控件
        controlstoggle(e) {
            if (!e.detail.show) {
                if (!this.xuanjiMask) {
                    this.isShowControl = false;
                }
                if (this.isFullscreen) {
                    this.showXuanji = true;
                }
                return;
            }
            this.isShowControl = true;
        },

        // 视频是否填充
        fillTap() {
            if (this.fill == 'fill') {
                this.fill = 'contain';
            } else {
                this.fill = 'fill';
            }
        },

        // 设置倍速播放
        playbackRate() {
            if (this.playbackRateIndex == this.playbackRateList.length - 1) {
                this.playbackRateIndex = 0;
                this.videoContext.playbackRate(Number(this.playbackRateList[this
                    .playbackRateIndex]));
                return;
            }
            this.playbackRateIndex++;
            this.videoContext.playbackRate(Number(this.playbackRateList[this
                .playbackRateIndex]));
        },
        onWaiting() {},
        isSameDay(timeStampA, timeStampB) {
            let dateA = new Date(timeStampA);
            let dateB = new Date(timeStampB);
            return dateA.setHours(0, 0, 0, 0) == dateB.setHours(0, 0, 0, 0);
        },
        onPlay: util.throttle(function(currentTime) {
            let _this = this;
            _this.VodLoadText = '';
            if (_this.$isMpWeixin) {
                // PC端不要广告
                if (_this.$u.sys().model == 'microsoft') {
                    return true;
                }
                
                // 会员无广告
                if (_this.sysconf.no_ad_limit > 0 && _this.sysconf
                    .userinfo && _this.sysconf.no_ad_limit_arr &&
                    _this.sysconf.no_ad_limit_arr.indexOf(_this.sysconf.userinfo.group_id) != -1) {
                    return true;
                }

                let ad_num = uni.getStorageSync('ad_num'),
                    now_time = new Date().getTime(),
                    end = new Date(new Date(new Date()
                            .toLocaleDateString()).getTime() + 24 * 60 *
                        60 * 1000 -
                        1)
                    .getTime();
                ad_num = ad_num ? ad_num : {
                    num: 0,
                    exp_time: now_time
                };
                // 如果达到设定次数，并且未到重置时间，则不看广告
                if (ad_num.num >= _this.sysconf.play_ad_num && _this
                    .sysconf.play_ad_switch) {
                    if (_this.isSameDay(ad_num.exp_time, end)) {
                        return true;
                    } else {
                        ad_num.num = 0;
                    }
                }
                if (!_this.isShowxx) {
                    // 总开关
                    if ((_this.sysconf.play_ad_control == 1 && _this
                            .playAdControl == 1) || _this.isAdShow) {
                        // 开播广告
                        if (_this.showAdFirst && !_this.isShowModel) {
                            _this.videoContext.pause();
                            _this.showModel(now_time, ad_num);
                            setTimeout(v => {
                                _this.videoContext.pause();
                            }, 1000);
                        }
                        // 插播次广告
                        if (parseInt(currentTime) >= _this
                            .halfTimeShowAd && (_this.showAdHalf || !
                                _this
                                .isShowModel)) {
                            _this.videoContext.pause();
                            _this.showModel(now_time, ad_num);
                            setTimeout(v => {
                                _this.videoContext.pause();
                            }, 1000);
                        }

                    }
                }

            }
        }, 800),
        showModel: util.throttle(function(now_time, ad_num) {
            let _this = this;
            _this.isShowModel = true;
            clearTimeout(_this.watingPlayTimer);
            _this.videoContext.pause();
            uni.showModal({
                title: _this.sysconf.play_ad_title,
                content: _this.sysconf.play_ad_text,
                confirmText: globalData.$t('detail').tryText,
                showCancel: _this.sysconf.should_play_end ?
                    false : true,
                success(res) {
                    _this.showAdFirst = 0;
                    _this.videoContext.pause();
                    _this.videoContext.exitFullScreen();
                    if (res.confirm) {
                        uni.setStorageSync('ad_num', {
                            num: parseInt(ad_num.num +
                                1),
                            exp_time: now_time
                        });
                        if (rewardedVideoAd && !_this.froms) {
                            rewardedVideoAd.show().then(() => {
                                _this.isAdShow = true;
                                if (_this
                                    .isFullscreen) {
                                    setTimeout(v => {
                                        uni.showModal({
                                            title: '提示',
                                            content: globalData
                                                .$t(
                                                    'detail'
                                                )
                                                .videoAdTips,
                                            showCancel: false,
                                            success() {
                                                _this
                                                    .videoContext
                                                    .exitFullScreen();
                                            }
                                        })
                                    }, 1000);
                                }
                            }).catch(() => {
                                rewardedVideoAd
                                    .load()
                                    .then(() =>
                                        rewardedVideoAd
                                        .show())
                                    .catch(err => {
                                        uni.showToast({
                                            title: globalData
                                                .$t(
                                                    'detail'
                                                )
                                                .noSpeedHallText,
                                            icon: 'none',
                                            success() {
                                                _this
                                                    .isAdShow =
                                                    0;
                                                _this
                                                    .playAdControl =
                                                    0;
                                                _this
                                                    .videoContext
                                                    .play();
                                            }
                                        });
                                    });
                            });
                        }
                    } else {
                        // 是否必须看完插屏广告才能播放
                        if (_this.sysconf.should_play_end ==
                            1 && _this.playAdControl == 1) {
                            _this.showAdHalf = 1;
                            _this.videoContext.play();
                        } else {
                            if (!_this.isClickCancel) {
                                _this.isClickCancel = true;
                            } else {
                                _this.showAdHalf = 0;

                            }
                            _this.videoContext.play();
                        }
                    }
                },
                complete() {
                    _this.videoContext.pause();
                }
            });
        }, 800),
        playing(event) {
            this.isPlaying = true;
            this.isShowPoster = false;
            this.isPause = false;

            // 没有历史记录
            if (!uni.getStorageSync(hex_md5(this.videoInfo.title + this.TabCur))) {
                MyApp.logHistory({
                    vod_pic: this.videoInfo.vod_pic,
                    title: this.videoInfo.title,
                    vod_id: this.vod_id,
                    index: this.TabCur,
                    type: this.videoInfo.type
                });
            }

            uni.setStorageSync(hex_md5(this.videoInfo.title + this.TabCur),
                event.detail.currentTime);
            this.VodLoadText = '';
            if (this.watingNum < 20) {
                this.playingNum = this.playingNum + 1;
            }
            this.playCurrentTime = event.detail.currentTime;
            if (parseInt(event.detail.currentTime) >= this.halfTimeShowAd &&
                this.showAdHalf) {
                this.onPlay(parseInt(event.detail.currentTime));
            }
        },
        playnext() {
            // 如果是剧集，播放完成自动播放下一集
            if (this.videoInfo.type != 1) {
                let index = this.TabCur + 1;
                if (index < this.videoInfo.srcList[this.vodPlayFromTab]
                    .length) {
                    this.currentTime = 0;
                    this.changeVideoInfo(index);
                }
            }
        },
        floatTouchClick(e) {
            switch (e) {
                case 1:
                    break;
                case 2:
                    uni.navigateBack({});
                    break;
            }
        },
        getSysConfig() {
            let _this = this;
            _this.$http.post('/videos/config/getConfig', {}).then(res => {
                let sys;
                if (res.code != 200) {
                    sys = uni.getStorageSync('sys_config');
                } else {
                    sys = res.data;
                    uni.setStorageSync('sys_config', res.data);
                }
                _this.sysconf = sys;
                _this.showCopy = _this.sysconf.copy_url;
                _this.isShowxx = _this.sysconf.show_xx;
                _this.saveModel = _this.sysconf.save_model;
                _this.playAdControl = _this.sysconf.play_ad_control;
                _this.showAdFirst = _this.sysconf.play_start_ad;
                _this.showAdHalf = _this.sysconf.show_ad_half;
                _this.halfTimeShowAd = _this.sysconf.show_ad_time;
                _this.loadingBoxText = globalData.centerShowxx ?
                    globalData.$t('detail').loadingText :
                    globalData.$t('detail').analysisText;
                // 会员等级
                _this.memberGroupLimit(res.data.userinfo);
                _this.getWechatMPSetting();
                // 激励广告
                if (_this.sysconf.play_ad_control == 1 && _this
                    .adInfo && _this.adInfo.play_start_ad
                    .length > 0) {
                    let index = _this.$func.randomNum(0, _this.adInfo
                        .play_start_ad.length - 1);
                    if (wx.createRewardedVideoAd && _this.adInfo &&
                        _this.adInfo.play_start_ad[index]
                        .hasOwnProperty('img')) {
                        rewardedVideoAd = wx.createRewardedVideoAd({
                            adUnitId: _this.adInfo
                                .play_start_ad[index].url
                        });
                        if (typeof rewardedVideoAd != 'undefined') {
                            rewardedVideoAd.onLoad(() => {
                                console.log('激励广告加载成功');
                            });
                            rewardedVideoAd.onError(err => {
                                uni.showToast({
                                    title: globalData
                                        .$t('detail')
                                        .noSpeedHallText,
                                    icon: 'none',
                                    success() {
                                        _this.isAdShow =
                                            0;
                                        _this
                                            .playAdControl =
                                            0;
                                        _this
                                            .videoContext
                                            .play();
                                    }
                                });
                                console.log('激励广告加载失败', err);
                            });
                            rewardedVideoAd.onClose(res => {
                                _this.isShowVideoTopAd = true;
                                setTimeout(v => {
                                    _this.isShowVideoTopAd = false;
                                }, 10000)
                                _this.showAdFirst = 0;
                                // 用户点击了【关闭广告】按钮
                                if (res && res.isEnded) {
                                    // 正常播放结束，可以下发游戏奖励
                                    _this.videoContext.play();
                                    _this.isAdShow = false;
                                    _this.playAdControl = 0;
                                } else {
                                    // 是否必须看完广告才能播放
                                    if (_this.sysconf
                                        .should_play_end == 1) {
                                        _this.showAdHalf = 1;
                                        _this.videoContext
                                            .play();
                                        uni.setStorageSync(
                                            'ad_num', {
                                                num: 0,
                                                exp_time: new Date()
                                                    .getTime()
                                            });
                                    } else {
                                        _this.showAdHalf = 0;
                                        _this.videoContext
                                            .play();
                                    }
                                }
                            });
                        }
                    }
                }
            }).then(v => {
                setTimeout(v => {
                    _this.playVideo();
                }, 100)
            });
        },
        getWechatMPSetting() {
            let _this = this;
            _this.init();
        },
        goBottomAd() {
            let _this = this;
            if (_this.$isAppPlus) {
                plus.runtime.openWeb(this.playerBottomAD.url);
            }
        },
        videoErrorCallback(e) {
            // console.log(e);
        },
        playerADCancel() {
            let _this = this;
            // 是否显示弹窗广告
            if (_this.sysconf.ad_close == 1) {
                _this.goAd();
            } else {
                _this.$refs['image'].close();
                _this.showAdModel = false;
                _this.drowVideoBox();
            }
        },
        init() {
            let _this = this;
            // 页面初始化
            _this.pageInit();
            // 广告
            _this.showAd();
        },
        memberGroupLimit(userinfo) {
            let _this = this;
            // 是否开启了会员等级控制
            if (_this.sysconf.member_group_switch) {
                // 如果当前不是审核状态
                if (!_this.isShowxx) {
                    // 如果当前登录会员等级低于后台设定等级，不予播放;如果未登录，不予播放；如果用户被锁定，不予播放
                    if (!userinfo.hasOwnProperty('group_id') || userinfo
                        .group_id < _this.sysconf
                        .member_group_limit || userinfo.islock) {
                        _this.isShowxx = true;
                        _this.pageInit();
                    } else {
                        _this.member_group = userinfo.group_id;
                        _this.froms = 0;
                    }
                } else {
                    // 如果当前已登录，是审核状态，但是会员等级高于设定等级并且未被锁定
                    if (userinfo.hasOwnProperty('group_id') && userinfo
                        .group_id >= _this.sysconf
                        .member_group_limit && !userinfo.islock) {
                        _this.member_group = userinfo.group_id;
                        _this.isShowxx = false;
                        _this.froms = 0;
                    }
                }
            } else {
                if (userinfo.hasOwnProperty('group_id') && userinfo.islock) {
                    _this.member_group = userinfo.group_id;
                    _this.isShowxx = true;
                }
                _this.froms = 0;
            }
        },
        pageInit() {
            let _this = this;
            const res = uni.getSystemInfoSync();
            _this.windowInfo.statusBarHeight = res.statusBarHeight;
            _this.windowInfo.windowHeight = res.windowHeight;
            _this.windowInfo.windowWidth = res.windowWidth;
            _this.windowInfo.trueHeight = _this.windowInfo.windowHeight - _this
                .windowInfo.statusBarHeight;
            let menuButtonInfo = uni.getMenuButtonBoundingClientRect();
            _this.windowInfo.trueHeight =
                _this.windowInfo.windowHeight - _this.windowInfo
                .statusBarHeight - menuButtonInfo.height;

            _this.bodyMinHeight = 'min-height:' + _this.windowInfo
                .windowHeight + 'px;overflow: hidden;';

            // 内容容器

            _this.contentStyle =
                'padding:0 10px;overflow-y:auto; z-index:0;position:relative; height: ' +
                (_this.windowInfo.trueHeight - _this.windowInfo.trueHeight /
                    2.5) +
                'px';
            if (_this.isShowxx) {
                _this.contentStyle =
                    'padding:0 10px;overflow-y:auto; z-index:0;position:relative;height: ' +
                    (_this.windowInfo.trueHeight - _this.windowInfo.trueHeight /
                        2.5) +
                    'px';
            }
            // 广告图片大小
            _this.playerADStyle =
                'z-index:10000;width: ' +
                (_this.windowInfo.windowWidth - 65) +
                'px; height: ' +
                (_this.windowInfo.trueHeight - 150) +
                'px;';
            _this.poster = _this.apiImageUrl + _this.sysconf.video_poster;
        },
        goAd() {
            this.playerADCancel();
        },
        getAdinfo() {
            let _this = this;
            _this.getSysConfig();
        },
        showAd() {
            let _this = this,
                index = 0;
            let sysconf = _this.sysconf;

            let adSwitchKg = globalData.adSwitchKg || uni.getStorageSync(
                'sys_config').ad_switch;
            if (adSwitchKg && sysconf.no_ad_limit_arr && sysconf.no_ad_limit_arr.indexOf(sysconf.userinfo.group_id) == -
                1) {
                // 是否显示弹窗广告
                if (sysconf.player_show_ad == 1 && _this.adInfo && _this.adInfo
                    .alert_ad.length > 0) {
                    index = _this.$func.randomNum(0, _this.adInfo.alert_ad
                        .length - 1);
                    if (_this.adInfo.alert_ad[index].hasOwnProperty('img')) {
                        if (_this.adInfo.alert_ad[index].ad_type < 3) {
                            _this.showAdModel = true;
                            _this.playerAd = _this.adInfo.alert_ad[index];
                            _this.$refs['image'].open();
                        } else {
                            // 原生
                            if (_this.adInfo.alert_ad[index].ad_type == 9) {
                                _this.customAD = _this.adInfo.alert_ad[index];
                                _this.customAD.img = _this.apiImageUrl + _this
                                    .adInfo.alert_ad[index].img;
                                _this.customAD.url = _this.adInfo.alert_ad[
                                    index].url;
                                _this.customAD.isshow = 1;
                            } else {
                                _this.playerBottomAD.isshow = 0;
                            }
                            // 插屏
                            if (_this.$isMpWeixin && !_this.froms && _this
                                .adInfo.alert_ad[index].ad_type ==
                                5) {
                                // 创建插屏组件
                                if (wx.createInterstitialAd) {
                                    interstitialAd = wx.createInterstitialAd({
                                        adUnitId: _this.adInfo.alert_ad[
                                            index].url
                                    });
                                    if (typeof interstitialAd != 'undefined') {
                                        interstitialAd.onLoad(() => {
                                            // 15秒内只能弹一次
                                            setTimeout(function() {
                                                interstitialAd
                                                    .show();
                                            }, 15200);
                                        });
                                        interstitialAd.onError(err => {});
                                        interstitialAd.onClose(() => {});
                                    }
                                }
                            }
                        }
                    }
                }
                // 顶部广告
                if (sysconf.player_bottom_ad == 1 && _this.adInfo && _this
                    .adInfo.video_top.length > 0) {
                    index = _this.$func.randomNum(0, _this.adInfo.video_top
                        .length - 1);
                    if (_this.adInfo.video_top[index].hasOwnProperty(
                            'img')) {
                        _this.playerTopAD = _this.adInfo.video_top[index];
                        _this.playerTopAD.img = _this.apiImageUrl + _this
                            .adInfo.video_top[index].img;
                        _this.playerTopAD.url = _this.adInfo.video_top[
                            index].url;
                        _this.playerTopAD.isshow = 1;
                    } else {
                        _this.playerTopAD.isshow = 0;
                    }
                }
                // 底部广告
                if (sysconf.player_bottom_ad == 1 && _this.adInfo && _this
                    .adInfo.alert_public.length > 0) {
                    index = _this.$func.randomNum(0, _this.adInfo.alert_public
                        .length - 1);
                    if (_this.adInfo.alert_public[index].hasOwnProperty(
                            'img')) {
                        _this.playerBottomAD = _this.adInfo.alert_public[index];
                        _this.playerBottomAD.img = _this.apiImageUrl + _this
                            .adInfo.alert_public[index].img;
                        _this.playerBottomAD.url = _this.adInfo.alert_public[
                            index].url;
                        _this.playerBottomAD.isshow = 1;
                    } else {
                        _this.playerBottomAD.isshow = 0;
                    }
                }
                // 播放前广告
                if (sysconf.show_video_ad == 1 && _this.adInfo && _this.adInfo
                    .video_ad.length > 0) {
                    index = _this.$func.randomNum(0, _this.adInfo.video_ad
                        .length - 1);
                    if (_this.adInfo.video_ad[index].hasOwnProperty('img')) {
                        if (_this.$isMp) {
                            _this.videoAd = _this.adInfo.video_ad[index].url;
                        }
                    }
                }
            }
            // 视频容器
            _this.drowVideoBox();
        },
        drowVideoBox() {
            this.videoStyle = 'background: rgb(0, 0, 0);width:100vw;height:' +
                this.windowInfo.trueHeight / 3 +
                'px;';
        },
        playVideo() {
            let _this = this;
            // 获取视频信息
            _this.$http
                .post('/videos/videos/getVideoDetail', {
                    vid: _this.vid,
                    index: _this.TabCur
                })
                .then(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    if (res.code != 200 || !res.data) {
                        uni.showModal({
                            content: res.data.msg || globalData.$t(
                                'detail').notFindVideoInfo,
                            showCancel: false,
                            success() {
                                uni.navigateBack();
                            }
                        });
                        return false;
                    }
                    _this.vod_id = res.data.vid;
                    _this.videoInfo.srcList = res.data.srcList;
                    _this.videoInfo.recommend = res.data.recommend;
                    _this.videoInfo.title = res.data.title;
                    _this.videoInfo.vod_pic = res.data.vod_pic;
                    _this.videoInfo.vod_time = res.data.vod_time;
                    _this.videoInfo.vod_year = res.data.vod_year;
                    _this.videoInfo.vod_actor = res.data.vod_actor;
                    _this.videoInfo.remark = res.data.remark;
                    _this.videoInfo.type = res.data.type;
                    _this.videoInfo.count = res.data.count;
                    _this.videoInfo.danmuList = res.data.danmuList;
                    _this.vodPlayFrom = res.data.vod_play_from;
                    _this.vodPlayFromTab = _this.vodPlayFrom[0];
                    _this.videoInfo.downLoadList = res.data.downLoadList;
                    _this.videoInfo.vod_score = res.data.vod_score;
                    _this.isLike = res.data.isLike;
                    _this.videoUrl = _this.videoInfo.srcList[_this
                        .vodPlayFromTab][0].url;
                    _this.currentTime = uni.getStorageSync(hex_md5(_this
                        .videoInfo.title + _this.TabCur));
                    // 转发分享
                    _this.share.title = _this.videoInfo.title;
                    _this.share.imageUrl = _this.videoInfo.vod_pic.search(
                            'http') >= 0 ?
                        _this.videoInfo.vod_pic : _this.imageUrl + _this
                        .videoInfo.vod_pic;
                    _this.share.desc = _this.videoInfo.title;
                    _this.share.content = _this.videoInfo.title;
                }).then(v => {
                    MyApp.closeLoading(_this.$refs.loading);
                    _this.changeVideoInfo(_this.TabCur)
                })
                .catch(err => {
                    MyApp.closeLoading(_this.$refs.loading);
                    _this.loading = false;
                });
        },
        tabSelect(e) {
            if (e.currentTarget.dataset.id != this.TabCur) {
                this.TabCur = e.currentTarget.dataset.id;
                // this.scrollLeft = (e.currentTarget.dataset.id - 1) * 60;
                this.currentTime = 0;
                this.playAdControl = 1;
                this.isShowModel = false;
                uni.setStorageSync(hex_md5(this.videoInfo.title + this.TabCur),
                    0);
                this.changeVideoInfo(e.currentTarget.dataset.id);
            }
        },
        tabPlayFromSelect(e) {
            let _this = this;
            if (e != _this.vodPlayFromTab) {
                _this.vodPlayFromTab = e;
                // _this.TabCur = -1;
                _this.changeVideoInfo(_this.TabCur);
                _this.cssAnimation = true;
                MyApp.showLoading(_this.$refs.loading, function() {
                    _this.cssAnimation = false;
                });
            }
        },
        changeVideo(index) {
            this.changeVideoInfo(index);
        },
        changeVideoInfo(index) {
            let _this = this;
            _this.changeYuan = true;
            _this.TabCur = index;
            if (this.videoContext) {
                this.videoContext.pause();
            }
            if (!((!_this.isShowxx || (_this.isShowxx && !_this.$isMp)) && !
                    _this.showAdModel && !_this
                    .froms) || _this
                .videoInfo.title) {
                _this.loading = false;
                MyApp.closeLoading(_this.$refs.loading);
            }

            // 转发分享
            _this.share.hard = 1;
            if (_this.shareTimeLineParams.indexOf('&tab=') == -1) {
                _this.shareTimeLineParams = _this.shareTimeLineParams +
                    '&tab=' + _this.TabCur;
            } else {
                _this.shareTimeLineParams = _this.shareTimeLineParams.replace(
                        /&tab=[0-9]/g, '&tab=') + _this
                    .TabCur;
            }
            if (_this.share.path.indexOf('&tab=') == -1) {
                _this.share.path = _this.share.path + '&tab=' + _this.TabCur;
            } else {
                _this.share.path = _this.share.path.replace(/&tab=[0-9]/g,
                    '&tab=') + _this.TabCur;
            }

            _this.videoUrl = '';
            _this.videoUrl = _this.videoInfo.srcList[_this.vodPlayFromTab][
                index
            ].url;
            let title =
                _this.videoInfo.title + (_this.isShowxx ? '' : ' - ' + _this
                    .videoInfo.srcList[_this
                        .vodPlayFromTab][
                        index
                    ].name);
            _this.navTitle = title;
            _this.share.title = title;
            uni.setNavigationBarTitle({
                title: title
            });
            clearTimeout(_this.watingPlayTimer);
            _this.watingNum = 1;
        },
        copyVideoUrl() {
            uni.setClipboardData({
                data: this.sysconf.save_player + this.trueVideoUrl,
                success: function() {
                    uni.showToast({
                        position: 'center',
                        title: '地址复制成功',
                        icon: 'none',
                        mask: true
                    })
                }
            });
        },
        report() {
            let _this = this,
                params = {
                    vod_play_from: _this.vodPlayFromTab,
                    vod_id: _this.vod_id,
                    vod_name: _this.videoInfo.title,
                    vod_index: _this.videoInfo.srcList[_this.vodPlayFromTab][
                        _this.TabCur
                    ].name,
                    remark: ''
                };
            uni.showModal({
                title: '提示',
                content: '如果当前视频无法播放，点击“一键反馈”给我们，我们将处理该问题',
                confirmText: '一键反馈',
                success: function(res) {
                    if (res.confirm) {
                        _this.$http
                            .post('/videos/report/report', params)
                            .then(res => {
                                if (200 == res.code) {
                                    uni.showToast({
                                        title: '反馈成功，感谢支持',
                                        mask: true,
                                        icon: 'none'
                                    })
                                }
                            })
                    }
                }
            })
        },
        picBtn() {
            let _this = this,
                params = {
                    // title: _this.videoInfo.title,
                    path: encodeURIComponent(_this.share.path),
                    // img: _this.videoInfo.vod_pic.search('http') >= 0 ?
                    //     _this.videoInfo.vod_pic : _this.imageUrl + _this.videoInfo.vod_pic,
                    encryptedData: 1,
                };
            _this.loadingBoxText = '正在处理...';
            MyApp.showLoading(_this.$refs.loading);
            _this.$http
                .post('/videos/config/createPoster', params)
                .then(res => {
                    MyApp.closeLoading(_this.$refs.loading);
                    _this.loading = false;
                    if (200 == res.code) {
                        _this.$refs.poster.showCanvas(_this.apiImageUrl +
                            res.data);
                    }
                }).catch(v => {
                    MyApp.closeLoading(_this.$refs.loading);
                    _this.loading = false;
                    uni.showModal({
                        title: '提示',
                        content: '生成海报失败，请联系客服',
                        showCancel: false
                    })
                })

        },
        kefuBtn() {
            let _this = this,
                tmp = [];
            tmp.push(_this.apiImageUrl + _this.sysconf.kefu_qrcode);
            uni.previewImage({
                urls: tmp,
                longPressActions: {
                    success: function(data) {

                    },
                    fail: function(err) {

                    }
                },
                complete(res) {

                }
            });
        },
        goAbout() {
            let type = uni.getStorageSync('sys_config').wxmp_btn_type;
            switch (type) {
                case '0':
                    uni.navigateTo({
                        url: '/pages/webview/about?url=' + encodeURIComponent(
                            this.sysconf.wxmp_url)
                    });
                    break;
                case '1':
                    let res = [],
                        image =
                        this.$config.apiImageUrl +
                        uni.getStorageSync('sys_config').wxmp_img;
                    res.push(image);
                    // 预览图片
                    uni.previewImage({
                        urls: res,
                        longPressActions: {
                            itemList: ['保存图片']
                        }
                    });
                    break;

            }

        },
        remarkme() {
            let _this = this;
            MyApp.checkLogin(true);
            let tpl = [];
            tpl.push(_this.sysconf.template_id);
            _this.loadingBoxText = '正在发送请求...';
            MyApp.showLoading(_this.$refs.loading);
            uni.requestSubscribeMessage({
                tmplIds: tpl,
                complete(res) {
                    if (res[_this.sysconf.template_id] == "accept") {
                        let params = {
                            vod_id: _this.vod_id,
                            vod_title: _this.videoInfo.title,
                            vod_time: _this.videoInfo.vod_time,
                            vod_score: _this.videoInfo.vod_score,
                            vod_year: _this.videoInfo.vod_year,
                            vod_actor: _this.videoInfo.vod_actor,
                        };
                        _this.$http
                            .post('/videos/subscribe/submit', params)
                            .then(res => {
                                MyApp.closeLoading(_this.$refs
                                    .loading);
                                _this.$u.toast(res.msg || (res
                                    .code == 200 ? '订阅成功' :
                                    '订阅失败'));
                            }).catch(v => {
                                MyApp.closeLoading(_this.$refs
                                    .loading);
                            })
                    } else {
                        MyApp.closeLoading(_this.$refs.loading);
                    }
                }
            })
        }
    }
};
